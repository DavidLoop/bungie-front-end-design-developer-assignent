
$(document).ready(function(){

// Superfish jQuery Menu: http://users.tpg.com.au/j_birch/plugins/superfish/
//-------------------------------------------------------------------------------------	
	$('ul.sf-menu').superfish({
		animation: {opacity: 'show', height: 'show'},
		speed: 'fast',
		hoverClass: 'hov',
		dropShadows: false,
		delay: 300
	});

// jQuery Flickr Feed: http://www.gethifi.com/blog/a-jquery-flickr-feed-plugin
// Colorbox: http://www.jacklmoore.com/colorbox
//-------------------------------------------------------------------------------------	
	$('#dogs').jflickrfeed({
		flickrbase: 'http://api.flickr.com/services/feeds/',
		feedapi: 'groups_pool.gne',
		limit: 12,
		qstrings: {
			id: '35034344814@N01'
		},
		itemTemplate: '<li>'+
						'<a rel="dogs" href="{{image}}" title="{{title}}">' +
							'<img src="{{image_s}}" alt="{{title}}" />' +
						'</a>' +
					  '</li>'
	}, function(data) {
		$('#dogs a').colorbox();
	});

	$('#cats').jflickrfeed({
		flickrbase: 'http://api.flickr.com/services/feeds/',
		feedapi: 'groups_pool.gne',
		limit: 12,
		qstrings: {
			id: '38559865@N00'
		},
		itemTemplate: '<li>'+
						'<a rel="cats" href="{{image}}" title="{{title}}">' +
							'<img src="{{image_s}}" alt="{{title}}" />' +
						'</a>' +
					  '</li>'
	}, function(data) {
		$('#cats a').colorbox();
	});

	$('#bunnies').jflickrfeed({
		flickrbase: 'http://api.flickr.com/services/feeds/',
		feedapi: 'groups_pool.gne',
		limit: 12,
		qstrings: {
			id: '480083@N22'
		},
		itemTemplate: '<li>'+
						'<a rel="bunnies" href="{{image}}" title="{{title}}">' +
							'<img src="{{image_s}}" alt="{{title}}" />' +
						'</a>' +
					  '</li>'
	}, function(data) {
		$('#bunnies a').colorbox();
	});

// jQuery Tools: http://jquerytools.org/
//-------------------------------------------------------------------------------------	
	$('ul.tabs').tabs("div.panes > ul", {
		current: 'current'
	});

});
